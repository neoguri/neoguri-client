<?php


namespace App\GitLab;


use Exception;
use function is_null;

class CommentBuilder
{
    private $previous_comment;

    public function __construct(ApiClient $client)
    {
        $comments = $client->get_comments();
        $this->previous_comment = $this->parse_bot_comment($comments);
    }

    private function parse_bot_comment($comments)
    {
        // TODO Parse bot-id from env and filter
        return $comments[0];
    }

    public function comment_id()
    {
        return $this->previous_comment['id'];
    }

    public function previous_comment_content()
    {
        return $this->previous_comment['body'];
    }

    public function update(string $new_comment_contents)
    {
        $prev =$this->previous_comment_content();

        return $this->prepend_bot_info($new_comment_contents);
    }

    private function bot_id()
    {
        $bot_id = env('NEOGURI_BOT_ID');

        if (is_null($bot_id)) {
            throw new Exception('No Neoguri Bot ID set! Please set the NEOGURI_BOT_ID env variable...');
        }

        return $bot_id;
    }

    private function prepend_bot_info(string $content)
    {
        $bot_id = $this->bot_id();

        return "<!-- NEOGURI_BOT $bot_id -->\n"
            . "<!-- PLEASE DO NOT REMOVE THE ABOVE LINE AS IT IS USED TO IDENTIFY THIS MESSAGE! -->\n"
            . "\n"
            . $content;
    }
}
