<?php

namespace App\GitLab;

use GuzzleHttp\Client;
use function json_decode;
use function urlencode;

class ApiClient
{
    private $host = '';
    private $namespace = '';
    private $project = '';
    private $mr = null;
    private $token = '';

    /** @var Client */
    private $http = null;

    public function __construct(
        string $host,
        string $namespace,
        string $project,
        int $mr,
        string $token
    )
    {
        $this->host = $host;
        $this->namespace = $namespace;
        $this->project = $project;
        $this->mr = $mr;
        $this->token = $token;

        $this->http = new Client([
            'headers' => [
                'PRIVATE-TOKEN' => $this->token,
            ],
        ]);
    }

    public function get_comments()
    {
        $url = $this->mr_url('/notes');
        return json_decode($this->http->get($url)->getBody()->getContents(), true);
    }

    public function update_comment(string $new_comment_contents)
    {
        $comment_builder = new CommentBuilder($this);
        $bot_comment_id = $comment_builder->comment_id();
        $updated_comment = $comment_builder->update($new_comment_contents);


        $this->http->put($this->mr_url("/notes/$bot_comment_id"), [
            'form_params' => [
                'body' => $updated_comment,
            ],
        ]);
    }

    protected function mr_url($path = '')
    {
        return $this->host
            . '/api/v4/projects/'
            . urlencode($this->project_with_namespace())
            . '/merge_requests/'
            . $this->mr
            . $path;
    }

    public function project_with_namespace() {
        if ($this->namespace === '') {
            return $this->project;
        }

        return $this->namespace . '/' . $this->project;
    }
}
