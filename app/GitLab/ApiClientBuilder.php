<?php


namespace App\GitLab;


use function compact;
use function env;
use function is_int;
use function mb_strlen;

class ApiClientBuilder
{
    private const GITLAB_HOST = 'GITLAB_HOST';
    private const GITLAB_PROJECT_NAMESPACE = 'GITLAB_PROJECT_NAMESPACE';
    private const GITLAB_PROJECT = 'GITLAB_PROJECT';
    private const GITLAB_MR_ID = 'GITLAB_MR_ID';
    private const GITLAB_API_TOKEN = 'GITLAB_API_TOKEN';

    private $host = '';
    private $namespace = '';
    private $project = '';
    private $mr = null;
    private $token = '';

    private $_errors = [];

    public function __construct()
    {
        $this->read();
    }

    public function errors()
    {
        return $this->_errors;
    }

    public function validate()
    {
        $this->_errors = [];

        if (mb_strlen($this->project) === 0) {
            $param = static::GITLAB_PROJECT;
            $this->_errors[] = "'$param' has to be defined as an "
                . 'environment-variable containing the name of the project!';
        }

        if (is_int($this->mr)) {
            $param = static::GITLAB_MR_ID;
            $this->_errors[] = "'$param' has to be defined as an "
                . 'environment-variable containing the numeric id of the merge request!';
        }

        if (mb_strlen($this->token) === 0) {
            $param = static::GITLAB_API_TOKEN;
            $this->_errors[] = "'$param' has to be defined as an "
                . 'environment-variable containing the api-token of your GitLab-instance!';
        }

        return $this->has_errors();
    }

    public function read()
    {
        $this->host = env(static::GITLAB_HOST, 'https://gitlab.com');
        $this->namespace = env(static::GITLAB_PROJECT_NAMESPACE, '');
        $this->project = env(static::GITLAB_PROJECT);
        $this->mr = intval(env(static::GITLAB_MR_ID));
        $this->token = env(static::GITLAB_API_TOKEN);
    }

    public static function build()
    {
        $builder = new static();
        $builder->validate();

        return new ApiClient(
            $builder->host,
            $builder->namespace,
            $builder->project,
            $builder->mr,
            $builder->token
        );
    }

    public function has_errors()
    {
        return count($this->_errors) > 0;
    }
}
