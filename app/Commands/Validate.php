<?php

namespace App\Commands;

use App\GitLab\ApiClientBuilder;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class Validate extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'validate';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Validates the environment.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $builder = new ApiClientBuilder();
        $builder->validate();

        if ($builder->has_errors()) {
            $this->comment('The following errors were found:');

            foreach ($builder->errors() as $error) {
                $this->error("  - " . $error);
            }

            return -1;
        }

        $this->info('Everything seems valid!');

        return 0;
    }
}
