<?php

namespace App\Commands;

use App\GitLab\ApiClientBuilder;
use Exception;
use function file_get_contents;
use Illuminate\Support\Facades\Artisan;
use function is_file;
use LaravelZero\Framework\Commands\Command;

class CommentCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = '
        comment
        {--content= : The content of the new comment. (optional)}
        {--file= : A path to the file containing the new comment.}
    ';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Updates the bot-comment.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        Artisan::call('validate');

        $client = ApiClientBuilder::build();

        $client->update_comment($this->new_comment_content());
    }

    private function new_comment_content()
    {
        $content_from_flag = $this->option('content');

        if ($content_from_flag) {
            return $content_from_flag;
        }

        $file_path = $this->hasOption('file')
            ? $this->option('file')
            : '/NEOGURI_BOT_COMMENT';

        if (is_file($file_path)) {
            return file_get_contents($file_path);
        }

        throw new Exception('No comment was provided!');
    }
}
